# Python 2/3 compatibility imports
from __future__ import print_function
from tkinter import E
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))
    
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

robot = moveit_commander.RobotCommander()

scene = moveit_commander.PlanningSceneInterface()

group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

# We can get the name of the reference frame for this robot:
planning_frame = move_group.get_planning_frame()
print("============ Planning frame: %s" % planning_frame)

# We can also print the name of the end-effector link for this group:
eef_link = move_group.get_end_effector_link()
print("============ End effector link: %s" % eef_link)

# We can get a list of all the groups in the robot:
group_names = robot.get_group_names()
print("============ Available Planning Groups:", robot.get_group_names())

# Sometimes for debugging it is useful to print the entire state of the
# robot:
print("============ Printing robot state")
print(robot.get_current_state())
print("")


# Planning to a Joint Goal
joint_goal = move_group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = -tau / 4
joint_goal[2]= tau / 5
joint_goal[3] = 0
joint_goal[4] = 0
joint_goal[5] = 0

move_group.go(joint_goal, wait=True)
move_group.stop()


# CARTESIAN PATH EXERCISE, like q3 move arm in E shaped initial

waypoints = []
scale = 1.0

wpose = move_group.get_current_pose().pose

wpose.position.z += scale * 0.3 
waypoints.append(copy.deepcopy(wpose))

wpose.position.x -= scale * 0.1
waypoints.append(copy.deepcopy(wpose))

wpose.position.z -= scale * 0.1
waypoints.append(copy.deepcopy(wpose))

wpose.position.x += scale * 0.1
waypoints.append(copy.deepcopy(wpose))

wpose.position.x -= scale * 0.1
waypoints.append(copy.deepcopy(wpose))

wpose.position.z -= scale * 0.1
waypoints.append(copy.deepcopy(wpose))

wpose.position.x += scale * 0.1
waypoints.append(copy.deepcopy(wpose))

wpose.position.x -= scale * 0.1
waypoints.append(copy.deepcopy(wpose))

(plan, fraction) = move_group.compute_cartesian_path(
    waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
)  # jump_threshold


# # Display trajectory
display_trajectory = moveit_msgs.msg.DisplayTrajectory()
display_trajectory.trajectory_start = robot.get_current_state()
display_trajectory.trajectory.append(plan)
# Publish
display_trajectory_publisher.publish(display_trajectory)

# Execute Plan
move_group.execute(plan, wait=True)



# POSE GOAL EXERCISE

# pose_goal = geometry_msgs.msg.Pose()
# pose_goal.orientation.w = 1.0
# pose_goal.position.x = 0.4
# pose_goal.position.y = 0.1
# pose_goal.position.z = 0.4

# move_group.set_pose_target(pose_goal)

# ## Now, we call the planner to compute the plan and execute it.
# # `go()` returns a boolean indicating whether the planning and execution was successful.
# success = move_group.go(wait=True)
# # Calling `stop()` ensures that there is no residual movement
# move_group.stop()
# # It is always good to clear your targets after planning with poses.
# # Note: there is no equivalent function for clear_joint_value_targets().
# move_group.clear_pose_targets()


# # Schmoving around w/ POSE

# next_goal = geometry_msgs.msg.Pose()
# next_goal.position.x = 1
# next_goal.position.y = -0.5
# next_goal.position.z = 0.8

# move_group.set_pose_target(next_goal)
# success = move_group.go(wait=True)
# move_group.stop()
# move_group.clear_pose_targets()

# # Difficulty in understanding position always from base and avoiding singularity

# next2_goal = geometry_msgs.msg.Pose()
# next2_goal.position.x = 0.8
# next2_goal.position.y = -0.5
# next2_goal.position.z = 0.8

# move_group.set_pose_target(next2_goal)
# success = move_group.go(wait=True)
# move_group.stop()
# move_group.clear_pose_targets()