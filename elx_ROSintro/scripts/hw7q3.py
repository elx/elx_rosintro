# Python 2/3 compatibility imports
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

robot = moveit_commander.RobotCommander()

scene = moveit_commander.PlanningSceneInterface()

group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)


# Create a singular line in the E initial
def add_wpose_E_line(wpose, waypoint):
    wpose.position.x += scale * 0.2 
    waypoints.append(copy.deepcopy(wpose))

    wpose.position.x -= scale * 0.2
    waypoint.append(copy.deepcopy(wpose))

# We can get the name of the reference frame for this robot:
planning_frame = move_group.get_planning_frame()
print("============ Planning frame: %s" % planning_frame)

# We can also print the name of the end-effector link for this group:
eef_link = move_group.get_end_effector_link()
print("============ End effector link: %s" % eef_link)

# We can get a list of all the groups in the robot:
group_names = robot.get_group_names()
print("============ Available Planning Groups:", robot.get_group_names())

# Sometimes for debugging it is useful to print the entire state of the
# robot:
print("============ Printing robot state")
print(robot.get_current_state())
print("")


# Planning to a Joint Goal
joint_goal = move_group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = -tau / 4
joint_goal[2]= tau / 5
joint_goal[3] = 0
joint_goal[4] = 0
joint_goal[5] = 0

move_group.go(joint_goal, wait=True)
move_group.stop()

# Cartesian path to move EE
waypoints = []
scale = 1.0

wpose = move_group.get_current_pose().pose

wpose.position.x -= scale * 0.2  # First move backwards in x for first line in E
waypoints.append(copy.deepcopy(wpose))

wpose.position.z -= scale * 0.1  # Second down in z for spine of E
waypoints.append(copy.deepcopy(wpose))

add_wpose_E_line(wpose, waypoints)

wpose.position.z -= scale * 0.1  # Move down in z for last portion of E spine
waypoints.append(copy.deepcopy(wpose))

add_wpose_E_line(wpose, waypoints)

# We want the Cartesian path to be interpolated at a resolution of 1 cm
# which is why we will specify 0.01 as the eef_step in Cartesian
# translation.  We will disable the jump threshold by setting it to 0.0,
# ignoring the check for infeasible jumps in joint space, which is sufficient
# for this tutorial.
(plan, fraction) = move_group.compute_cartesian_path(
    waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
)  # jump_threshold


# # Display trajectory
display_trajectory = moveit_msgs.msg.DisplayTrajectory()
display_trajectory.trajectory_start = robot.get_current_state()
display_trajectory.trajectory.append(plan)
# Publish
display_trajectory_publisher.publish(display_trajectory)

# Execute Plan
move_group.execute(plan, wait=True)

